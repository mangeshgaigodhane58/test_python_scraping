class MetaCheckProcessAttribute(type):
    def __init__(cls, name, bases, attrs):
        process_method = attrs.get('process')
        if callable(process_method):
            # Check if 'process' method has exactly 3 parameters
            parameters = inspect.signature(process_method).parameters
            if len(parameters) != 3:
                raise TypeError(f"'process' method in class '{name}' must take exactly 3 arguments")
        else:
            raise TypeError(f"'{name}' must have a 'process' method")
        super().__init__(name, bases, attrs)


import inspect

class MyClass1(metaclass=MetaCheckProcessAttribute):
    def process(self, arg1, arg2, arg3):
        pass

class MyClass2(metaclass=MetaCheckProcessAttribute):
    def process(self, arg1, arg2):
        pass  # This will raise a TypeError

class MyClass3(metaclass=MetaCheckProcessAttribute):
    pass  # This will raise a TypeError
